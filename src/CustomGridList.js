import React, { Component } from 'react';

import image from './its-the-end-of-the-world-not-the-moon.jpg';

import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTitleBar from '@material-ui/core/GridListTileBar';
import TextField from '@material-ui/core/TextField';

class CustomGridList extends Component {
  renderGridListTiles() {
    let gridListTiles = [];
    for (let i = 0; i < 8; i++) {
      const gridListTile = (
        <GridListTile key={i}>
          <img src={image} alt="Image" />
          <GridListTitleBar title={`Test ${i}`} />
        </GridListTile>
      );

      gridListTiles.push(gridListTile);
    }
    return gridListTiles;
  }

  render() {
    return (
      <Grid container justify="center">
        <Grid item xs={6}>
          <form>
            <TextField
              label="Search field"
              value=""
              margin="normal"
              variant="outlined"
              fullWidth
            />
          </form>
        </Grid>
        <Grid item xs={12} className="top-space">
          <GridList cellHeight={180} cols={3} spacing={16}>
            {this.renderGridListTiles()}
          </GridList>
        </Grid>
      </Grid>
    );
  }
}

export default CustomGridList;
